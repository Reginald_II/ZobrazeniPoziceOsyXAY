package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class HlavniOkno extends JFrame{
	CanvasOkna canvasOkna;
	
	public HlavniOkno() {
		super();
		setSize(800,600);
		setTitle("Pozice osy X a Y");
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		//Definice severu � M�sta pro vykreslov�n�
		canvasOkna = new CanvasOkna();
		canvasOkna.setSize(getWidth(),getHeight());
		canvasOkna.setLayout(new BoxLayout(canvasOkna,BoxLayout.LINE_AXIS));
		
		
		//Definice jihu � V�pisu pozice X a Y po kliknut�
		JPanel jih = new JPanel();
		jih.setLayout(new BoxLayout(jih,BoxLayout.LINE_AXIS));
		
		JLabel poziceX = new JLabel();
		JLabel poziceY = new JLabel();
		
		poziceX.setFont(new Font("Arial",Font.BOLD,12));
		poziceY.setFont(new Font("Arial",Font.BOLD,12));
		
		jih.add(poziceX);
		jih.add(Box.createRigidArea(new Dimension(10,0)));
		jih.add(poziceY);
		
		//Definice mouse listeneru pro klik�n� na pozici
		canvasOkna.addMouseListener(new MouseAdapter() {
			
					
			@Override
			public void mousePressed(MouseEvent e) {
				System.out.println("Pozice x: "+e.getX()+"; Pozice y: "+e.getY());
				poziceX.setText("Pozice x: "+e.getX());
				poziceY.setText("Pozice y: "+e.getY());
				
			}
			
		});
		
		//Definice mouse listeneru pro zobrazov�n� pozic X a Y na z�klad� p�ejezdu kurzorem my�i
		canvasOkna.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				poziceX.setText("Pozice x: "+e.getX());
				poziceY.setText("Pozice y: "+e.getY());
			}
		});
		
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
			if(e.getKeyCode()==KeyEvent.VK_ESCAPE) {
				System.exit(0);
			}
			}
		});
		
		add(canvasOkna,BorderLayout.CENTER);
		add(jih,BorderLayout.SOUTH);
		setVisible(true);
	}
	
}
