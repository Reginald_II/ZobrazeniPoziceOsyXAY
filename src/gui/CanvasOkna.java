package gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class CanvasOkna extends JPanel{
	
	public CanvasOkna() {
		super();
	}
	@Override
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
	}

}
